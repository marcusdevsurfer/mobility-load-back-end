package com.devcoast.mobilityloadbackend.entity;

import jakarta.persistence.*;

import java.util.List;

@Entity(name = "carriers")
public class Carrier {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "carrier_name")
    private String name;

    @Column(name = "carrier_email")
    private String email;

    @Column(name = "carrier_address")
    private String address;

    @Column(name = "carrier_city")
    private String city;

    public Carrier(String name, String email, String address, String city) {
        this.name = name;
        this.email = email;
        this.address = address;
        this.city = city;
    }

    public Carrier() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
